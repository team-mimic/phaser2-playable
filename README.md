Phaser 2 - Playable Ad
====

Basic template to create playable ads using Phaser 2.

## Sublime Text

Sublime Text project is included with a custom build system. Use `Ctrl+B` to build a inlined `index.html` file inside a `build` folder.

# Changelog

## v 1.0.0

Initial release.