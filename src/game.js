
const game = new Phaser.Game(
	window.innerWidth * window.devicePixelRatio,
	window.innerHeight * window.devicePixelRatio,
	Phaser.AUTO,
	'',
	{
		preload: preload,
		create: create,
		update: update
	}
);

var centerX;
var centerY;

var scene;

var colorsGroup;
var patternsGroup;

var label;

function preload()
{
	console.log("Preloading and setting things up...");

	scene = this;
	centerX = game.width * 0.5;
    centerY = game.height * 0.5;

	loadAudio();
    loadAtlas();
    loadFonts();

    setupInput();
}

function loadAtlas()
{
	game.load.atlas('atlas', atlasURI, "", atlasData, Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
}

function loadAudio()
{
	game.load.audio('bounce', [bounceSoundURI]);
}

function loadFonts()
{
	game.load.bitmapFont('roboto', fontTextureURI, "", fontXML);
}

function setupInput()
{
	game.input.onTap.add(onTap, this);
}

function create()
{
	game.physics.startSystem(Phaser.Physics.ARCADE);

	colorsGroup = game.add.group();
	colorsGroup.enableBody = true;
	colorsGroup.physicsBodyType = Phaser.Physics.ARCADE;

	patternsGroup = game.add.group();
	patternsGroup.enableBody = true;
	patternsGroup.physicsBodyType = Phaser.Physics.ARCADE;

	spawnColor();
	spawnPattern();

	label = game.add.bitmapText(0, 0, 'roboto', 'Collisions: 0');
}

function spawnColor()
{
    var sprite = colorsGroup.create(centerX, centerY, 'atlas', '02');

    game.physics.enable(sprite);

    sprite.body.velocity.setTo(game.rnd.integerInRange(-500, 500), game.rnd.integerInRange(500, -500));
    sprite.body.collideWorldBounds = true;
    sprite.body.bounce.set(1, 1);
}

function spawnPattern()
{
	var sprite = patternsGroup.create(centerX, centerY, 'atlas', 'pattern');

	game.physics.enable(sprite);

    sprite.body.velocity.setTo(game.rnd.integerInRange(-500, 500), game.rnd.integerInRange(500, -500));
    sprite.body.collideWorldBounds = true;
    sprite.body.bounce.set(1, 1);
}

function update()
{
	// Check for collisions between colors and patterns
	game.physics.arcade.collide(colorsGroup, patternsGroup, onCollision, null, this);

	label.text = "Collisions: " + collisionsCount;
}

var collisionsCount = 0;

function onCollision(color, pattern)
{
	console.log("Collision!");

	game.sound.play('bounce');

	collisionsCount++;
}

function onTap(pointer, doubleTap)
{
	if (doubleTap)
	{
		spawnPattern();
	}
	else
	{
		spawnColor();
	}
}
